'use strict'
const mysql = require('mysql');

/// Cached connection with dbms
let connection = null;

var pool = null;

const get_db_connection = (callback) => {
    pool.getConnection((err, connection) => {
        if (err) {
            console.log(err);
            callback(null);
            return;
        }
        callback(connection)
    })
}


export const connect = (host, user, password, database, callback) => {
    pool = mysql.createPool({
        host: host,
        user: user,
        password: password,
        database: database
    });
    callback();
}

export const begin_transaction = (query, callback) => {
    get_db_connection((connection) => {
        connection.beginTransaction((err) => {
            connection.query(query, (err, result, fields) => {
                if (err) {
                    return connection.rollback(() => {
                        callback(err);
                        return;
                    })
                }
            })
            connection.commit((err) => {
                if (err) {
                    return connection.roolback(() => {
                        callback(err)
                        return;
                    });
                }
                callback(err);
            });
        })
        connection.release();
    })
}

export const rollbackAction = () => {
    return connection.roolback();
}

const Connected = (): boolean => {
    if (connection != null) {
        return connection.state != 'disconnected' ? true : false;
    }
    return false;
}


export const query = (what, from, extra = '', callback = null) => {
    get_db_connection((connection) => {
        let query = `SELECT ${what} FROM ${from} ${extra}`
        console.log(query);
        connection.query(query, (err, result, fields) => {
            if (err) {
                console.log(err);
                callback(err, null)
                return;
            }
            callback(null, result);
            connection.release()
        });
    })
}

export const insert = (where, what, value, callback) => {
    let query = `INSERT INTO ${where} (${what}) VALUES (${value})`;
    console.log(query);
    begin_transaction(query, (err) => {
        callback(err);
    });
}

export const insert_s = (where, what, value, callback) => {
    get_db_connection((connection) => {
        let query = `INSERT INTO ${where} (${what}) VALUES (${value})`;
        console.log(query);
        connection.query(query, (err, result, fields) => {
            if (err) {
                return connection.rollback(() => {
                    callback(err, null);
                })
            }
            callback(err, result);
        })
        connection.release();
    })
}

export const deleteRow = (from, where, callback) => {
    
    let query = `DELETE FROM ${from} WHERE ${where}`;
    console.log(query);
    begin_transaction(query, (err) => {
        callback(err);
    })
}

export const deleteRow_s = (from, where, callback) => {
    get_db_connection((connection) => {
        let query = `DELETE FROM ${from} WHERE ${where}`;
        connection.query(query, (err, result, fields) => {
            if (err) {
                return connection.rollback(() => {
                    callback(err, null);
                })
            }
            callback(err, result);
            connection.release();
        })
    })
}

export const updateRow = (table, where, set, callback) => {
    let query = `UPDATE ${table} SET ${set} WHERE ${where}`
    console.log(query);
    begin_transaction(query, (err) => {
        callback(err);
    })
}

export const disconnect = () => {
    connection.disconnect();
}