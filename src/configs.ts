/**
 * SMTP configuration class
 */
export class SmtpConfig{
    public serverHost :String = "";
    public serverUser : String = "";
    public serverPassword : String = "";
    public serverPort : Number = 587;
    public secure : boolean = false;
    public minVersion : String = "TLSv1"; 
    public ciphers : String = "HIGH:MEDIUM:!aNULL:!eNULL:@STRENGTH:!DH:!kEDH";

    constructor(host, user, password){
        this.serverHost = host;
        this.serverUser = user;
        this.serverPassword = password
    }
}
