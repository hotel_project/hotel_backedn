'use strict'

import * as fs from 'fs';
import * as multer from 'multer';

const upload = multer({ dest: process.env.upload_folder })

export const upload_file = (filename) =>{
    return upload.single(filename);
}

export const remove_file_from_temp = (file_name) =>{
    fs.unlink(file_name, (err) => {
        if(err)
            console.log(err);
    });
}
