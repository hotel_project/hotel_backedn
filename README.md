# hotel_backedn

### Setup

Required mysql server!

Install packages

``` npm install ```

#### Envoiroment setup
Create a .env file on the root of the project, and insert the following parameters

```
server_address = 127.0.0.1
server_port = 3000

#Database fields
username = "usename"
password = "password"
database = "db_name"
host = "localhost"
file_table = "table??"

#SMTP settings
serverHost = hostname
serverUser = email
serverPassword = pwd
```

### Run server
``` npm start ```
