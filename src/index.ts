import { server_init, map_get, map_post_upload, map_post } from "./server";
import * as dotenv from "dotenv";
dotenv.config()
/// Load enviroment variables
import { 
    add_room,
    createNewBooking, 
    getRoom, 
    getRoomReservations, 
    getRooms, 
    get_first_room, 
    remove_room 
} from './api';

import { connect } from './rdbms_clinet'
import { auth, get_user_by_token, verify, add_user, get_user_details } from "./auth";


/// ##################################
/// --- setup all routin down here....
/// ##################################

map_get('/get_rooms', getRooms);

map_get('/get_room_reservations', getRoomReservations);

map_post('/submit_booking', createNewBooking);

map_get('/get_room', getRoom);

map_post('/add_user', add_user);

map_post('/get_user_details', get_user_details);

map_post('/add_room', add_room);

map_get('/get_first_room', get_first_room);

map_post('/remove_room', remove_room);

/**
 * Auth
 */
///login
map_post("/login", auth);
///token verification
map_post('/get_user_by_token', get_user_by_token);

map_get('/verify', verify);

/**
 * server initialization
 */

/// Connecting to sql server
try {
    connect(process.env.host, process.env.dbusername, process.env.dbpassword, process.env.database, (err) => {
        if (err) {
            throw new Error("Fail to connecting to the database : " + err);
        }
        /// Setup listening connection
        server_init(process.env.server_port, process.env.server_address, () => {
            console.log("Started on http://" + process.env.server_address + ":" + process.env.server_port);
        });
    });
}
catch (e) {
    console.log(e);
}
/**
 * unexpected error 
 */
process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});
