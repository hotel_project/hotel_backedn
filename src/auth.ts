/**
 * This call contain all the function for the authringincation
 * and verifing token
 */

import * as fs from 'fs';
import {insert, insert_s, query} from './rdbms_clinet';
const jwt = require("jsonwebtoken");
import * as bcryptjs from 'bcryptjs';
/**
 * ########################
 * ---- Authentication ----
 * ########################
 */

/**
 * Authenticate the user if the input email is present in the db and it is enabled.
 * @param {*} req
 * @param {*} res
 */
export const auth = (req, res) => {
    let email = req.body.email;
    let password = req.body.password
    query('*', 'credentials', `WHERE email='${email}'`,(err,data) =>{
        if (err) console.log(err);
        if (data.length == 0) return res.sendStatus(401);
        let row = data[0];
        bcryptjs
            .compare(password, row.password)
            .then((passwordCorrect) => {
                if (!passwordCorrect){
                    console.log("incorrect password");
                    return res.sendStatus(401);
                } 
                try {
                    console.log("Login successfull");
                    let tokenMilliseconds = 1000 * 60 * 60 * parseInt(process.env.tokenHours);
                    let expiringIn = tokenMilliseconds;
                    console.log("EXPIRE TOKEN IN =",expiringIn);
                    fs.readFile(process.env.RSA_KEY, (err, key) => {
                        if (err) {
                            console.log(err);
                            return res.send(false);
                        }
                        let token = jwt.sign({
                            email: req.body.email,
                            role_value : row.type
                        }, key, {
                            algorithm: "RS256",
                            expiresIn: expiringIn,
                        });
                        res.send({
                            token: token
                        });
                    });
                } catch (err) {
                    console.log(err);
                    req.end();
                }
            });
    })
};

export const get_user_details = (req, res) => {
    let userId = req.body._id;
    query('*', 'users', `WHERE credential_id=${userId}`, (err, data) =>{
        if(err){
            console.log(err);
            res.status(502).send("Error....");
            return;
        }
        res.status(200).send(data[0]);
    })
}

export const verify = (req, res) => {
    verify_intern(req.query.token, (data) =>{
        if(data != null)
            res.end(data)
        else{
            res.status(502).send('Error processing your request');
        }
    } );
};

export const verify_intern = (token, callback) => {
    let resultObject = {
        logged: false,
        email: "",
        role_value : -1
    };
    if(token == undefined || token == '')
        return callback(JSON.stringify(resultObject));
    fs.readFile(process.env.RSA_PUBLIC_KEY, (readErr, data) => {
        jwt.verify(token, data, {
            algorithms: ["RS256"]
        }, (verifyErr, decoded) => {
            if (verifyErr) {
                console.log(verifyErr);
                return callback(JSON.stringify({
                    logged: false,
                    user: "not logged"
                }));
            }
            resultObject.logged = true;
            resultObject.email = decoded.email; 
            resultObject.role_value = decoded.role_value;
            callback(JSON.stringify(resultObject));
        });
    });
};

const crypt_password = (pwd, callback) => {
    bcryptjs.hash(pwd, 10).then((hash) => {
      callback(hash);
    });
  };

export const add_user = (req, res) => {
    let email = req.body.email;
    let password = req.body.password;
    let name = req.body.name;
    let surname = req.body.surname;
    let cf = req.body.cf;
    query('email', 'credentials', `WHERE email='${email}'`, (err, data)=>{
        if(err){
            console.log(err);
            res.status(502).send("Error processing your request");
            return;
        }
        if(data != null){
            crypt_password(password, (hash) => {
                insert_s('credentials', 'email, password', `'${email}', '${hash}'`,(err, data) =>{
                    if(err){
                        console.log(err);
                        res.status(502).send("Error processing your request");
                        return;
                    }
                    insert_s('users', 'name, surname, cf, credential_id', `'${name}', '${surname}', '${cf}', '${data.insertId}'`, (err, data)=>{
                        if(err){
                            console.log(err);
                            res.status(502).send("Error processing your request");
                            return;
                        }
                        res.status(200).send("User added correctly");
                    })
                })
            })
        }
        else{
            res.status(502).send("This email is already in use");
        }
    })
};

export const get_user_by_token = (req, res) => {
    let token = req.body.token;
    if (token == undefined) {
      res.sendStatus(403);
      return;
    }
    verify_intern(token, (usrSession) => {
      const usrSessionJson = JSON.parse(usrSession);
      console.log('islogged: ' + usrSession.logged);
      if (!usrSessionJson.logged) 
        res.sendStatus(403);
      else {
        query('*', 'credentials', `WHERE email='${usrSessionJson.email}'`,(err, data) => {
          if (err) {
            console.log(err);
            res.sendStatus(403);
          } else if (data.length == 0) {
            console.log("user not found");
            res.sendStatus(403);
          } else {
            res.send(data[0]);
            res.end();
          }
        });
      }
    });
};