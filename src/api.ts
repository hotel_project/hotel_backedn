import { query, insert, deleteRow, deleteRow_s, insert_s } from './rdbms_clinet';

/**
 * Api functions here...
 */

export const getRooms = (req, res) => {
    query('*', 'rooms', '', (err, data) => {
        if (err) {
            res.status(400).send("Request failed");
            return;
        }
        res.status(200).send(data);
    })
}

export const getRoom = (req, res) => {
    query('*', 'rooms', `WHERE _id=${req.query._id}`, (err, data) => {
        if (err) {
            res.status(400).send("Request failed");
            return;
        }
        res.status(200).send(data[0]);
    })
}

export const createNewBooking = (req, res) => {
    query('_id', 'users', `WHERE credential_id=${req.body.id_client}`, (err, data) => {
        if (err) {
            console.log(err);
            res.status(400).send("Error searching the following user");
            return;
        }
        insert_s(
            'booking',
            'd_start, d_end, id_client, id_room',
            `'${req.body.d_start}', '${req.body.d_end}', ${data[0]._id}, ${req.body.id_room}`, (err, date) => {
                if (err) {
                    console.log(err)
                    res.status(400).send("Error addeing the current reservation");
                    return;
                }
                res.sendStatus(200);
            })
    })
}


export const add_room = (req, res) => {
    let roomData = req.body;
    let values = `'${roomData.name}', ${roomData.n_bed}, '${roomData.description}', '${roomData.mediaLink}'`
    insert('rooms', 'name, n_bed, description, mediaLink', values, (err, data) => {
        if (err) {
            res.status(400).send(err);
            return;
        }
        res.sendStatus(200);
    });
}

export const remove_room = (req, res) => {
    let room_id = req.body._id;
    deleteRow_s('rooms', `_id=${room_id}`, (err, data) => {
        if (err) {
            console.log(err);
            res.status(400).send(err);
            return;
        }
        res.sendStatus(200);
    })
}

export const get_first_room = (req, res) => {
    query('*', 'rooms', 'LIMIT 4', (err, data) => {
        if (err) {
            console.log(err);
            res.status(400).send("error");
            return;
        }
        res.status(200).send(data);
    })
}

export const getRoomReservations = (req, res) => {
    let roomId = req.query._id;
    query('*', 'booking', `WHERE id_room=${roomId}`, (err, data) => {
        if (err) {
            console.log(err);
            res.status(400).send("error");
            return;
        }
        res.status(200).send(data);
    })
}
