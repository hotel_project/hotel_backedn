// import express_jwt from 'express-jwt';
// import fs from 'fs';

import * as express from "express";
import * as bodyParser from "body-parser";
import * as cors from "cors";

import * as Sentry from "@sentry/node";
import * as Tracing from "@sentry/tracing";

const app = express();

const corsOptions = {
    origin: "*",
    methods: 'GET, HEAD, PUT, PATCH, POST, DELETE'
};

//  const checkIfAuthenticated = express_jwt({
//    secret: fs.readFileSync("key.rsa.pub"),
//    algorithms: ["RS256"]
//  });

///set cors options
app.use(cors(corsOptions));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));



Sentry.init({
    dsn: "https://5ee48d7ce00045139b30aa0fd1cea5f0@o541017.ingest.sentry.io/5790598",
    integrations: [
      // enable HTTP calls tracing
      new Sentry.Integrations.Http({ tracing: true }),
      // enable Express.js middleware tracing
      new Tracing.Integrations.Express({ app }),
    ],
  
    // Set tracesSampleRate to 1.0 to capture 100%
    // of transactions for performance monitoring.
    // We recommend adjusting this value in production
    tracesSampleRate: 1.0,
  });

// RequestHandler creates a separate execution context using domains, so that every
// transaction/span/breadcrumb is attached to its own Hub instance
app.use(Sentry.Handlers.requestHandler());
// TracingHandler creates a trace for every incoming request
app.use(Sentry.Handlers.tracingHandler());



/**
 * ##########################
 * ---- Generics Methods ----
 * ##########################
 */

/**
 * Initialization method for server
 * @param {Number} port
 * @param {String} address
 * @param {Function} callback
 */
export const server_init = (port, address, callback) => {
    ///setup address and port
    app.listen(port, address, callback);
};

/**
 * Generic method to create a server get route
 * @param {String} route
 * @param {Function} handler
 */
export const map_get = (route, handler) => {
    app.get(route, (req, res) => {
        handler(req, res);
    });
};
/**
 * Generic method to create a server put route
 * @param {String} route
 * @param {Function} handler
 */
export const map_put = (route, handler) =>{
    app.put(route, (req, res) =>{
        handler(req, res);
    });
}
/**
 * Generic method to create a server delete route
 * @param {String} route
 * @param {Function} handler
 */
export const map_delete = (route, handler) =>{
    app.delete(route, (req, res) =>{
        handler(req, res);
    });
}
/**
 * Generic method to create a server post route
 * @param {String} route
 * @param {Function} handler
 */
export const map_post = (route, handler) => {
    app.post(route, (req, res) => {
        handler(req, res);
    });
};

/**
 * 
 * @param {*} route 
 * @param {*} handler 
 * @param {*} uploadHandler 
 */
export const map_post_upload = (route, handler, uploadHandler) => {
    app.post(route, uploadHandler, (req, res) => {
        handler(req, res);
    });
};

/**
 *
 * @param {*} route
 * @param {*} handler
 */
// export const map_post_auth = (route, handler) => {
//     app.post(route, checkIfAuthenticated, (req, res) => {
//         handler(req, res);
//     });
// };

/**
 *
 * @param {*} route
 * @param {*} handler
 */
// export const map_get_auth = (route, handler) => {
//     app.get(route, checkIfAuthenticated, (req, res) => {
//         handler(req, res);
//     });
// };
