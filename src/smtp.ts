const nodemailer = require("nodemailer");
import { SmtpConfig } from "./configs";

/// Setup smtp configuration
let config = new SmtpConfig(
    process.env.serverHost,
    process.env.serverUser,
    process.env.serverPassword
);

/// Create smtp transport
const transporter = nodemailer.createTransport({
    host: config.serverHost,
    port: config.serverPort,
    secure: config.secure,
    auth: {
        user: config.serverUser,
        pass: config.serverPassword,
    },
    tls: {
        minVersion: config.minVersion,
        ciphers: config.ciphers,
    },
});

export const sendMail = (options) => {
    transporter.sendMail(options, (err, response) => {
        if (err) {
            console.log(err);
        }
        console.log('email sended');
    });
};

export const example = () => {
    const mailOptions = {
        from: 'test@gmail.com', // sender address
        to: 'nicholasfal19@gmail.com', // list of receivers
        subject: "TEST", // Subject line
        html: 'asdadwa', // html body
    };
    sendMail(mailOptions);
}